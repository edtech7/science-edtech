import React, {useState} from 'react';
import { Routes, Route, useLocation} from 'react-router-dom';
import Home from "./pages/Home.js";
import Chemistry from "./pages/Chemistry.js";
import Biology from "./pages/Biology.js";
import Physics from "./pages/Physics.js";
import Admin from "./pages/Admin.js";
import User from "./pages/User.js";
import Login from "./pages/Login.js";
import Logout from "./pages/Logout.js";
import AppNavbar from "./components/AppNav/AppNavbar";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
function App() {
  return (
    <div className="fullWindowWidth">
      <AppNavbar />
      <Routes>
        <Route path="/"       element={ <Home /> } />
        <Route path="/chem"   element={ <Chemistry /> } />
        <Route path="/bio"    element={ <Biology   /> } />
        <Route path="/phys"   element={ <Physics   /> } />
        <Route path="/admin"  element={ <Admin     /> } />
        <Route path="/user"   element={ <User      /> } />
        <Route path="/login"  element={ <Login     /> } />
        <Route path="/logout" element={ <Logout    /> } />
      </Routes>
    </div>
  );
}

export default App;
