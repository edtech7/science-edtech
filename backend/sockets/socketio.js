const http = require("http");
const userController = require("../controllers/userFunctions.js");
const commons = require("../controllers/commonFunctions.js");
const User = require("../models/User.js");
const socketDB = require("../models/SocketConnections.js");



module.exports.mySockets = (server) => {

	User.updateMany({"connectionSocket": {$gt: 0}}, {"connectionSocket": []}).then((ok, err) => {
		console.log(ok);
	})


	const io = require("socket.io")(server, {
		cors: {
			origin: ["https://science-edtech.net", "http://localhost:3000"],
			methods: ["GET", "POST", "PUT", "DELETE"],

		}
	})

	io.on('connection', (socket) => {

		  socket.on('disconnect', () => {
		  	console.log(`${socket['email']} disconnected`);
		    userController.updateUser({find: {email: socket['email']}, update: { $pull: { connectionSocket: socket.id }}})
		  });

		  socket.on('chat message', (msg) => {
		    socket.broadcast.emit('chat message', msg);
		    console.log(msg)
		  });

		  socket.on('user', (email) => {
		  	socket['email'] = email;
		  	console.log(`${socket['email']} connected`);
		  	userController.updateUser({find: {email: email}, update: { $push: { connectionSocket: socket.id }}})
		  })

		  socket.on('disconnectUser', (email, socketId) => {
			userController.updateUser({find: {email: email}, update: { $pull: { connectionSocket: socketId }}})
		  })

		});
}

