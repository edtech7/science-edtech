const MNG = require("mongoose");

//data should contain toFind and toUpdate in cases of updating methods
//data should be an Object


module.exports.find = (a, data, sort, limit, project) => {
	return a.aggregate().match(data).sort(sort).limit(limit).project(project).then(ok => {
		if (ok && ok.length > 0)
			return ok;
		else
			return { error: `No data found.`};
	})
}

module.exports.findOne = (a, data, type) => {
	return a.findOne(data).then(ok => {
		if (ok.length > 0)
			return ok;
		else
			return notFound(type)
	})
}

module.exports.update = (a, data, type) => { //by ID
	return a.findOneAndUpdate(data.find, data.update).then(ok => {
		if (ok && ok.length > 0)
			return { message: `${type} successfully updated.` };
		else
			return notFound(type)
	})
}

module.exports.delete = (a, data, type) => { //by ID
	return a.findByIdAndDelete(data).then(ok => {
		if (ok) 
			return { message: `${type} successfully deleted.`};
		else
			return notFound(type);
	})
}

module.exports.indexer = async(a, data) => { // data is the name of the index
	return a.findOne({name: data}).then(ok => {
		if (ok)
			ok.lastIndx += 1
			ok.save();
			return ok.lastIndx;
	})
}

module.exports.findByIndex = async(a, data) => {
	return a.findOne(data).then(ok => {
		if (ok)
			return ok;
		else
			return notFound("Index");
	})
}


notFound = (type) => {return {error : `${type} not found`}}