const bcrypt = require("bcrypt");
const env = require("dotenv").config();
const USER = require("../models/User.js");
const pnv = process.env;
const jwt = require("jsonwebtoken");
const commons = require("./commonFunctions.js");

//module specific functions
	
	module.exports.createUser = async (data) => {
		let user = new USER();

		if (data.pass != data.vPass)
			return {error: "Password mismatch"}
		if (data.email != "" && data.pass != "" && data.pass != undefined) {
			if(data.pass.length < 8)
				return {error: "Password is too short"};
			user.email = data.email.toLowerCase();
			user.password = passHash(data.pass);
		}
		user.fName = data.fName;
		user.mName = data.mName;
		user.lName = data.lName;
		user.phone = data.phone;
		user.fullName = data.fName + " " + data.mName + " " + data.lName;
		user.bDate = new Date(data.bday);
		user.position = data.position;

		return await user.save().then(ok => {
			if (ok){

				return {message: "User successfully saved."};
			}
		}).catch(err => {
			console.log(err)
			if(err.code === 11000)
				return {"error": "Email is already in use"}
			else{
				return {"error": "Please provide input for all fields."};
			}
		})
	}

	passHash = pass => {return bcrypt.hashSync(pass, parseInt(pnv.SALT));}

	module.exports.loginUser = async (data) => {
		email = data.email;
		password = data.password;
		if (email && password) {
			return USER.findOne({email: email}).then((result, error) => {
				if(result){
					return bcrypt.compare(password, result.password).then(result2 => {
						if (result2) {
							expiry  = new Date();
							iat = expiry.getTime() / 1000
							expiry.setTime(expiry.getTime() + 18000000)
							let forToken = mkTKN(JSON.parse(`{"email": "${result.email}", "isAdmin": "${result.isAdmin}","position": "${result.position}", "iat": ${iat}}`))
							return JSON.parse(`{
								"message": "login successful",
								"token": "${forToken}",
								"fName": "${result.fName}",
								"mName": "${result.mName}",
								"lName": "${result.lName}",
								"email": "${result.email}",
								"isAdmin": "${result.isAdmin}",
								"position": "${result.position}",
								"expiry": "${expiry}",
								"id": "${result._id}"
							}`);
						}
						else{
							return JSON.parse('{"error": "Invalid login credentials"}');
						}
					})
				
				}
			})
		}
		else
			return ({"error": "Please provide input for all fields"})
	}

	mkTKN = (data) => {
		return jwt.sign(data, pnv.SECRET, { expiresIn: '6h' })
	}

	module.exports.updateUser = async (data) => { // takes in email and update data.
		return commons.update(USER, data, "User")
	}

//generic database functions