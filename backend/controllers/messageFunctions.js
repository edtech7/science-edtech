const commons = require('./commonFunctions.js');
const USER = require('../models/User.js');
const CHAT = require('../models/Chat.js');

module.exports.createChat = async (data) => {
	const chat = new CHAT();

	chat.participants = data.participants;
	chat.isPrivate = data.isPrivate;
	chat.lastModified = new Date();

	return await chat.save().then(ok => {
		console.log(ok);
	}).catch(err => {
		console.log(err);
	})
}

module.exports.searchChat = async (data) => {

	let keys = Object.keys(data);
	let values = Object.values(data);
	let query = {};

	for (let i = 0; i < keys.length; i++) 
		query[keys[i]] = { $regex: values[i], $options: "$i" }

	return commons.find(USER, query, "fullName", 10, { 'fullName': 1 }).then(ok => {
		console.log(ok)
		return ok;
	})
}