const FRM = require("../models/Forum.js");
const controller = require("./commonFunctions.js");

module.exports.createForumPost = async (data) => {
	let post = new FRM();
	post.author = data.author;
	post.content = data.content;
	post.title = data.title;
	post.category = data.category;	
	console.log(post)

	return await post.save().then(ok => {
		if (ok) 
			console.log("ok")
			return { message: "Successfully posted" };
	}).catch(err => {
		console.log(err);
		if (err) {
			return { error: "There was an problem posting. Please Try again." };
		}
	})
}

module.exports.forumPostList = (data, sort, limit) => {
		return controller.find(FRM, data, sort, limit).then(ok => {
		if (ok)
			return ok;
		else
			return { error: "An error occured."}
	})
} 		