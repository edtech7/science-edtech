const exp = require("express")
const controller = require("../controllers/userFunctions.js");
const route = exp.Router();

module.exports = route;

route.post('/register', (req, res) => { controller.createUser(req.body).then(result => res.send(result)) })

route.post('/login', (req, res) => { controller.loginUser(req.body).then(result => res.send(result))})

route.put('/update', (req, res) => { controller.updateUser(req.body).then(result => res.send(result))})