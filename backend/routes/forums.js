const EXP = require("express");
const route = EXP.Router();
const controller = require("../controllers/forumfunctions.js");

module.exports = route;

route.post("/forumPost", (req, res) => { controller.createForumPost(req.body).then(result => res.send(result)) })

route.get("/forumhome-:type-:size", (req,res) => { controller.forumPostList({category: req.params.type}, {lastModified: -1}, req.params.size).then(result => { res.send(result)}) })