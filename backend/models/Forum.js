const MNG = require("mongoose");

const ForumPostSchema = new MNG.Schema({	
	author: {
		type: String,
		required: [true, "Author is required"]
	},
	lastModified: {
		type: Date,
		default: Date.now
	},
	title: {
		type: String,
		required: [true, "Title is required"],
		maxLength: 60
	},
	content: {
		type: String,
		required: [true, "Content is required"],
		minLength: 30
	},
	category: String,
	views: {
		type: Number,
		default: 0
	},
	upvotes: {
		type: Number,
		default: 0
	},
	downvotes: {
		type: Number,
		default: 0
	},
	replies: [{

		author: {
			type: String,
			required: [true, "Author is required"]
		},
		count: 0,
		content: {
			type: String,
			minLength: 20
		}
	}],
	hidden: {
		type: Boolean,
		default: false
	}
})

const FORUM = MNG.model("Forum Post", ForumPostSchema);
module.exports = FORUM;