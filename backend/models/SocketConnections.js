const mng = require('mongoose');

const socketConnectionSchema = new mng.Schema({
	userID: String
});

const SocketConnection = mng.model("Socket Connection", socketConnectionSchema);
module.exports = SocketConnection;