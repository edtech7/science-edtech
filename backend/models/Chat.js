const MNG = require('mongoose');

const chatSchema = new MNG.Schema({
	participants: [{
		email: String
	}],
	isPrivate: Boolean,
	lastModified: Date,
	dateCreated: {
		type: Date,
		default: new Date()
	},
	chat: {
		content: String,
		sender: String,
		dateSend: Date
	}

})

const CHAT = MNG.model("CHAT", chatSchema);
module.exports = CHAT;
