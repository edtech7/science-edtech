const mng = require("mongoose");

const userSchema = new mng.Schema({
	email: {
		type: String,
		immutable: true,
		unique: [true, "Email is already in use"]
	},
	password: {
		type: String,
		required: [true, "Password is required"],
		minLength: [8, "Password should be 8 characters long"]
	},
	fName: {
		type: String,
		required: [true, "First Name is required"]
	},
	mName: {
		type: String
	},
	lName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	fullName: String,
	bDate: {
		type: Date,
		required: [true, "Birth date is required"],

	},
	phone: {
		type: String,
		unique: [true, "Phone number is already in use"]
	},
	address: {
		region: {
			name: String,
			re2_code: String,
		},
		prov: {
			name: String,
			prov_code: String,
		},
		mun: {
			name: String,
			mun_code: String 
		},
		brgy: {
			name: String,
		}
	},
	zip: {
		type: String
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date(),
		immutable: true
	},
	position: {
		type: String
	},
	connectionSocket: [{
		type: String
	}],
	conversations: [{
		roomId: String,
		lastModified: Date
	}]

});


const USER = mng.model("USER", userSchema);
module.exports = USER;