//const declarations
	const MNG = require("mongoose");
	const EXP = require("express");
	const CRS = require("cors");
	const env = require("dotenv").config();
	const pnv = process.env;
	const app = EXP();
	const http = require("http");
	const server = http.createServer(app)
	const socketController = require("./sockets/socketio.js");

	server.listen(pnv.PORT, (req, res) => console.log(`Server listening at Port ${pnv.PORT}`))
	
	const USR = require("./routes/users.js");
	const FRM = require("./routes/forums.js");
	const MSG = require("./routes/messages.js");
	


//app use functions
	app.use(CRS())
	app.use(EXP.json());
	app.use("/users", USR);
	app.use("/forums", FRM);
	app.use("/chat", MSG);



MNG.connect(pnv.DB);
MNG.connection.once("open", (req, res) => console.log(`Database connection established`));


//socket.io
	socketController.mySockets(server);


app.get('/', (req, res) => res.send("MSilvan"));
app.get('/test', (req, res) => res.send("hello"));

//test(s: string)