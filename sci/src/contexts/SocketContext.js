import React from 'react';

const SocketContext = React.createContext();
export const SocketContextProvider = SocketContext.Provider;
export default SocketContext;