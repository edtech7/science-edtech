import React from 'react';

export default function Page404() {
	return(
		<h1>Error: The page you're looking for couldn't be found.</h1>
	)
}